import { Component } from '@angular/core';
import { Data } from './mock'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	public data 
	private draggingID
	private draggingIndex

	constructor(){
		this.data = Data
	}		

	dragDossier(ev, id, i) {
		this.draggingID = id
		this.draggingIndex = i
    ev.dataTransfer.setData("text", ev.target.id);
    ev.target.className += " dragging"
  }
  overDossier(ev){
  	 ev.preventDefault();
  	 ev.target.className += " dossierOver"	
  }
  leaveDossier(ev){
  	ev.target.classList.remove('dossierOver');	
  }
  dropDossier(ev, item){
  	if(!item.isFolder){
  		item.isFolder = true
  	}
  	if(item.isFolder) {
  		this.data = this.data.filter(d => d.id !== this.draggingID)
  	}
  	ev.target.classList.remove('dossierOver');	
  }


  overMiddle(ev){
  	ev.preventDefault();
  	ev.target.classList.add('over')	
  }
  leaveMiddle(ev){
  	ev.target.className = "middle"		
  }
  dropMiddle(ev, i){
  	this.reorder(this.draggingIndex, i)
  	ev.target.className = "middle"	
  }

  reorder(from, to){
  	if(from > to){
  		to += 1 
  	}
  	let arr = this.data
  	var element = arr[from];
    arr.splice(from, 1);
    arr.splice(to, 0, element);
    this.data = arr
  }

}