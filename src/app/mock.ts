export const Data = [
	{
		id: 1,
		value: "A",
		isFolder: false, 
	},
	{
		id: 2,
		value: "B",
		isFolder: true, 
	},
	{
		id: 3,
		value: "C",
		isFolder: false, 
	},
	{
		id: 4,
		value: "D",
		isFolder: false, 
	},
	{
		id: 5,
		value: "E",
		isFolder: false, 
	},
	{
		id: 6,
		value: "F",
		isFolder: false, 
	}
]